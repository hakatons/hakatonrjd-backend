package ru.nikitavov.ticketai.service.dl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nikitavov.ticketai.database.model.Ticket;
import ru.nikitavov.ticketai.database.repository.TicketRepository;
import ru.nikitavov.ticketai.service.sheet.service.DataPoint;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DataGen {
    private final TicketRepository ticketRepository;

    public List<DataPoint> getData() {
        List<DataPoint> result = new ArrayList<>();
        List<Ticket> all = ticketRepository.findAll();
        for (Integer year : all.stream().map(Ticket::getYear).distinct().toList()) {
            for (Integer month : all.stream().filter(ticket -> ticket.getYear().equals(year)).map(Ticket::getMonth).distinct().toList()) {
                for (Integer day : all.stream().filter(ticket -> ticket.getYear().equals(year)).filter(ticket -> ticket.getMonth().equals(month))
                        .map(Ticket::getDay).distinct().toList()) {
                    for (Integer stationId : all.stream().filter(ticket -> ticket.getYear().equals(year))
                            .filter(ticket -> ticket.getMonth().equals(month)).filter(ticket -> ticket.getDay().equals(day))
                            .map(ticket1 -> ticket1.getStationStart().getId()).distinct().toList()) {
                        int count = all.stream().filter(ticket -> ticket.getYear().equals(year))
                                .filter(ticket -> ticket.getMonth().equals(month))
                                .filter(ticket -> ticket.getDay().equals(day))
                                .filter(ticket -> ticket.getStationStart().getId().equals(stationId))
                                .mapToInt(Ticket::getPlaceCount).sum();
                            result.add(new DataPoint(stationId, year, month, day, count));
//                        for (Integer count : all.stream().filter(ticket -> ticket.getYear().equals(year))
//                                .filter(ticket -> ticket.getMonth().equals(month))
//                                .filter(ticket -> ticket.getDay().equals(day))
//                                .filter(ticket -> ticket.getStationStart().getId().equals(stationId))
//                                .map(Ticket::getPlaceCount).toList()) {
//                            result.add(new DataPoint(stationId, year, month, day, count));
//                        }
                    }
                }
            }
        }
        return result;
        // Верните ваш набор данных в виде списка объектов DataPoint
    }

}
