package ru.nikitavov.ticketai.service.dl;

import lombok.RequiredArgsConstructor;
import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.stereotype.Service;
import ru.nikitavov.ticketai.service.sheet.service.DataPoint;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TicketDemandPrediction {

    private final DataGen dataGen;
    public MultiLayerNetwork model;

    public void main() {
        MultiLayerNetwork multiLayerNetwork = trainModel();
        model = multiLayerNetwork;
        System.out.println("Прогноз: " + predictDemand(multiLayerNetwork, new double[]{2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 16, 2022}));
    }


    public MultiLayerNetwork trainModel() {
        int numInputs = 15;
        int numOutputs = 1;
        int numHiddenUnits = 50;
        int batchSize = 1;
        int epochs = 25;

        List<DataPoint> dataPoints = dataGen.getData();

        double[][] inputs = new double[dataPoints.size()][numInputs];
        double[][] outputs = new double[dataPoints.size()][numOutputs];

        for (int i = 0; i < dataPoints.size(); i++) {
            DataPoint dataPoint = dataPoints.get(i);
            inputs[i][0] = dataPoint.getCity();
            double month = dataPoint.getMonth();
            for (int m = 0; m < 12; m++) {
                inputs[i][m + 1] = (m == month - 1) ? 1 : 0; // one-hot encoding for month
            }
            inputs[i][13] = dataPoint.getDay();
            inputs[i][14] = dataPoint.getYear();
            outputs[i][0] = dataPoint.getTicketCount();
        }

        INDArray inputArray = Nd4j.create(inputs);
        INDArray outputArray = Nd4j.create(outputs);

        DataSet dataSet = new org.nd4j.linalg.dataset.DataSet(inputArray, outputArray);
        DataSetIterator iterator = new ListDataSetIterator<>(dataSet.asList(), batchSize);

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(new Adam())
                .list()
                .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenUnits)
                        .activation(Activation.RELU).build())
                .layer(1, new DenseLayer.Builder().nIn(numHiddenUnits).nOut(numHiddenUnits)
                        .activation(Activation.RELU).build())
                .layer(2, new DenseLayer.Builder().nIn(numHiddenUnits).nOut(numHiddenUnits)
                        .activation(Activation.RELU).build())
                .layer(3, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .activation(Activation.IDENTITY).nIn(numHiddenUnits).nOut(numOutputs).build())
                .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();

        int listenerFrequency = 50;
        net.setListeners(new ScoreIterationListener(listenerFrequency));

        for (int i = 0; i < epochs; i++) {
            net.fit(iterator);
            iterator.reset();
        }

        net.rnnClearPreviousState();

        // Путь к файлу, в котором будет сохранена модель
        String filePath = "model.zip";

// Сохранение модели
        try {
            ModelSerializer.writeModel(net, filePath, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return net;
    }


    public double predictDemand(MultiLayerNetwork multiLayerNetwork, double[] inputData) {
        INDArray input = Nd4j.create(inputData, new int[]{1, inputData.length});
        INDArray output = multiLayerNetwork.output(input);
        return output.getDouble(0);
    }

    public void loadModel() {
        if (model != null) return;
        String filePath = "model.zip";
        try {
            model = ModelSerializer.restoreMultiLayerNetwork(filePath);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

