package ru.nikitavov.ticketai.service.sheet.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DataPoint {
    private double city;
    private double year;
    private double month;
    private double day;
    private double ticketCount;

    // конструкторы, геттеры, сеттеры...
}
