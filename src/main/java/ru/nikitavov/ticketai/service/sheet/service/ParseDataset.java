package ru.nikitavov.ticketai.service.sheet.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import ru.nikitavov.ticketai.database.model.*;
import ru.nikitavov.ticketai.database.repository.*;
import ru.nikitavov.ticketai.service.sheet.util.ExcelBookUtil;
import ru.nikitavov.ticketai.service.sheet.util.ExcelSheetUtil;

import java.io.File;
import java.time.YearMonth;
import java.util.*;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class ParseDataset {

    private final ExcelBookUtil bookUtil;
    private final ExcelSheetUtil sheetUtil;
    private final TrainRepository trainRepository;
    private final TrainThreadRepository threadRepository;
    private final StationRepository stationRepository;
    private final TrainTypeRepository typeRepository;
    private final TicketClassRepository classRepository;
    private final TicketRepository ticketRepository;

    public void parse() {
        XSSFWorkbook workbook = bookUtil.loadBook(new File("D:\\projects\\projectHak\\TicketAI\\src\\main\\resources\\static\\СВОД 2022.xlsx")).get();

        int actualYear = 2022;
        int lastYear = 2021;

        XSSFSheet sheet = workbook.getSheetAt(0);

        List<XSSFRow> rows = sheetUtil.getRows(sheet, 7);

        int actualMonth = 0;

        HashMap<Integer, ArrayList<XSSFRow>> rowsByMonth = new HashMap<>();

        for (XSSFRow row : rows) {
            if (row.getPhysicalNumberOfCells() == 2 || row.getPhysicalNumberOfCells() == 1 || row.getPhysicalNumberOfCells() == 0) {
                actualMonth++;
                continue;
            }

            if (actualMonth < 3) {
                rowsByMonth.computeIfAbsent(actualMonth, integer -> new ArrayList<>()).add(row);
                continue;
            }


            if (sheetUtil.getCell(row, 2).getNumericCellValue() > actualMonth) {
                actualMonth++;
            }

            rowsByMonth.computeIfAbsent(actualMonth, integer -> new ArrayList<>()).add(row);
        }

        for (Map.Entry<Integer, ArrayList<XSSFRow>> entry : rowsByMonth.entrySet()) {
            for (XSSFRow row : entry.getValue()) {

                String[] splitTrain = sheetUtil.getCell(row, 1).getStringCellValue().split("/");
                Train train = getOrCreateTrain(splitTrain[0], trainRepository, s -> Train.builder().name(s).build());
                TrainThread trainThread = getOrCreateTrain(splitTrain[1], threadRepository, s -> TrainThread.builder().name(s).build());

                Integer month = entry.getKey();
                Integer monthBuy = (int) sheetUtil.getCell(row, 2).getNumericCellValue();
                Station stationStart = getOrCreateTrain(sheetUtil.getCell(row, 3).getStringCellValue(), stationRepository, s -> Station.builder().name(s).build());
                Station stationEnd = getOrCreateTrain(sheetUtil.getCell(row, 4).getStringCellValue(), stationRepository, s -> Station.builder().name(s).build());

                String[] splitType = sheetUtil.getCell(row, 5).getStringCellValue().split("/");
                TrainType trainType = getOrCreateTrain(splitType[0], typeRepository, s -> TrainType.builder().name(s).build());
                TicketClass ticketClass = getOrCreateTrain(splitType[1], classRepository, s -> TicketClass.builder().name(s).build());

                Integer placeCount = (int) sheetUtil.getCell(row, 6).getNumericCellValue();
                Integer distance = (int) sheetUtil.getCell(row, 11).getNumericCellValue();

                Integer seatPrice = (int) sheetUtil.getCell(row, 8).getNumericCellValue();

                Ticket ticket = Ticket.builder()
                        .train(train)
                        .thread(trainThread)
                        .month(month)
                        .monthBuy(monthBuy)
                        .stationStart(stationStart)
                        .stationEnd(stationEnd)
                        .trainType(trainType)
                        .ticketClass(ticketClass)
                        .placeCount(placeCount)
                        .distance(distance)
                        .year(actualYear)
                        .yearBuy(monthBuy > month ? lastYear : actualYear)

                        .seatPrice(seatPrice)

                        .build();

                ticketRepository.save(ticket);
            }
        }

        for (int i = 1; i <= 12; i++) {
            YearMonth yearMonthObject = YearMonth.of(actualYear, i);
            int daysInMonth = yearMonthObject.lengthOfMonth();
            int actualDay = 0;
            List<Ticket> tickets = ticketRepository.findDistinctByYearAndMonth(actualYear, i);

            Map<Integer, Integer> map = new HashMap<>();

            for (Integer price : tickets.stream().map(Ticket::getSeatPrice).distinct().toList()) {
                map.put(price, (actualDay % daysInMonth) + 1);
                actualDay++;
            }

            for (Ticket ticket : tickets) {
                ticket.setDay(map.get(ticket.getSeatPrice()));
                ticketRepository.save(ticket);
            }

//            List<Integer> distinctByYearAndMonth = tickets.stream().map(Ticket::getSeatPrice).distinct().toList();
            System.out.println();
        }
    }

    public <T> T getOrCreateTrain(String name, NameRepository<T, ?> repository, Function<String, T> function) {
        Optional<T> optional = repository.findByName(name);
        if (optional.isPresent()) {
            return optional.get();
        }

        T apply = function.apply(name);
        return repository.save(apply);
    }
}
