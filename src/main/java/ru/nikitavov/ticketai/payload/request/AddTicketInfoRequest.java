package ru.nikitavov.ticketai.payload.request;

public record AddTicketInfoRequest(String ticketTrain, String ticketInfo, String ticketPrice) {
}
