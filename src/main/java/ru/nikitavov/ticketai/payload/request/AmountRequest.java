package ru.nikitavov.ticketai.payload.request;

public record AmountRequest(int amount) {
}
