package ru.nikitavov.ticketai.payload.request;

public record AiRequest(int stationId, int year, int month, int day) {
}
