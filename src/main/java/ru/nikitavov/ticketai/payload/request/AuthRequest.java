package ru.nikitavov.ticketai.payload.request;

public record AuthRequest(String login, String password) {
}
