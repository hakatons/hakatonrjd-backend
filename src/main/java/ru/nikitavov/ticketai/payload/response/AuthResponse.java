package ru.nikitavov.ticketai.payload.response;

public record AuthResponse(boolean isLogin) {
}
