package ru.nikitavov.ticketai.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nikitavov.ticketai.database.model.*;
import ru.nikitavov.ticketai.database.repository.*;
import ru.nikitavov.ticketai.payload.request.AmountRequest;
import ru.nikitavov.ticketai.security.data.CurrentUser;
import ru.nikitavov.ticketai.security.data.UserPrincipal;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("users")
public class UserController {

    private final UserRepository userRepository;
    private final CardRepository cardRepository;
    private final CardItemRepository cardItemRepository;
    private final DishRepository dishRepository;
    private final StatusRepository statusRepository;
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;

    @GetMapping
    public ResponseEntity<List<User>> all() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @GetMapping("me/{id}")
    public ResponseEntity<User> me(@PathVariable int id) {
        return ResponseEntity.ok(userRepository.findById(id).get());
    }

    @GetMapping("/me")
    public ResponseEntity<User> id(@CurrentUser UserPrincipal user) {
        return ResponseEntity.ok(userRepository.findById(user.getId()).get());
    }

    @GetMapping("{userId}/order")
    public ResponseEntity<List<Order>> getOrders(@PathVariable int userId) {
        return ResponseEntity.ok(orderRepository.findOrderByUser_Id(userId));
    }

    @GetMapping("order")
    public ResponseEntity<List<Order>> getAllOrders() {
        return ResponseEntity.ok(orderRepository.findAll());
    }

    @GetMapping("{userId}/card")
    public ResponseEntity<Card> addInCard(@PathVariable int userId) {
        return ResponseEntity.ok(getCardOrCreate(userId));
    }

    @PostMapping("{userId}/card/add/{id}")
    public ResponseEntity<Void> addInCard(@PathVariable int id, @PathVariable int userId) {
        Card card = getCardOrCreate(userId);
        Optional<CardItem> item = cardItemRepository.findByCard_IdAndDish_Id(card.getId(), id);
        if (item.isPresent()) return ResponseEntity.ok().build();
        cardItemRepository.save(CardItem.builder().card(card).dish(dishRepository.findById(id).get()).amount(1).build());
        return ResponseEntity.ok().build();
    }

    @PostMapping("{userId}/card/update/{id}")
    public ResponseEntity<User> updateInCard(@PathVariable int id, @PathVariable int userId, @RequestBody AmountRequest amount) {
        Card card = getCardOrCreate(userId);
        Optional<CardItem> item = cardItemRepository.findByCard_IdAndDish_Id(card.getId(), id);
        if (item.isEmpty()) return ResponseEntity.ok().build();
        item.get().setAmount(amount.amount());
        cardItemRepository.save(item.get());
        return ResponseEntity.ok().build();
    }

    @PostMapping("{userId}/card/remove/{id}")
    public ResponseEntity<User> removeFromCard(@PathVariable int id, @PathVariable int userId) {
        Card card = getCardOrCreate(userId);
        Optional<CardItem> item = cardItemRepository.findByCard_IdAndDish_Id(card.getId(), id);
        if (item.isEmpty()) return ResponseEntity.ok().build();
        cardItemRepository.delete(item.get());
        return ResponseEntity.ok().build();
    }

    @PostMapping("{userId}/card/create")
    public ResponseEntity<User> createOrder(@PathVariable int userId) {
        Card card = getCardOrCreate(userId);
        Order order = Order.builder().time(LocalTime.now()).user(userRepository.findById(userId).get()).status(statusRepository.findById(1).get()).build();
        orderRepository.save(order);
        card.getItems().stream().map(cardItem -> OrderItem.builder().order(order).dish(cardItem.getDish()).amount(cardItem.getAmount()).build())
                .forEachOrdered(orderItemRepository::save);
        cardItemRepository.deleteAll(card.getItems());
        return ResponseEntity.ok().build();
    }

    public Card getCardOrCreate(int userId) {
        User user = userRepository.findById(userId).get();
        Card card = user.getCard();
        if (card != null) return card;

        card = cardRepository.save(new Card());

        return card;
    }


}
