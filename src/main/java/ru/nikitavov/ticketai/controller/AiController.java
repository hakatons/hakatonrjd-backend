package ru.nikitavov.ticketai.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nikitavov.ticketai.payload.request.AiRequest;
import ru.nikitavov.ticketai.service.dl.TicketDemandPrediction;
import ru.nikitavov.ticketai.service.sheet.service.DataPoint;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("ai")
public class AiController {

    private final TicketDemandPrediction demandPrediction;

    @GetMapping("forecast")
    public ResponseEntity<Double> getResponse(AiRequest aiRequest) {

        demandPrediction.loadModel();
        double[] inputs = new double[15];
        DataPoint dataPoint = new DataPoint(aiRequest.stationId(), aiRequest.year(), aiRequest.month(), aiRequest.day(), 0);
        inputs[0] = dataPoint.getCity();
        double month = dataPoint.getMonth();
        for (int m = 0; m < 12; m++) {
            inputs[m + 1] = (m == month - 1) ? 1 : 0; // one-hot encoding for month
        }
        inputs[13] = dataPoint.getDay();
        inputs[14] = dataPoint.getYear();

        return ResponseEntity.ok(demandPrediction.predictDemand(demandPrediction.model, inputs));
    }
}
