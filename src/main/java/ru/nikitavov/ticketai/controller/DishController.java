package ru.nikitavov.ticketai.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nikitavov.ticketai.database.model.Dish;
import ru.nikitavov.ticketai.database.model.User;
import ru.nikitavov.ticketai.database.repository.DishRepository;
import ru.nikitavov.ticketai.database.repository.UserRepository;

import java.util.List;

@RestController
@RequestMapping("/dish")
@RequiredArgsConstructor
public class DishController {

    private final DishRepository dishRepository;

    @GetMapping
    public ResponseEntity<List<Dish>> all() {
        return ResponseEntity.ok(dishRepository.findAll());
    }

}
