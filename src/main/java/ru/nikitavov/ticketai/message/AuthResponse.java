package ru.nikitavov.ticketai.message;

public record AuthResponse(String accessToken) {

}
