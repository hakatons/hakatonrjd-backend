package ru.nikitavov.ticketai.message;

public record SignUpRequest(String name, String login, String password, String phone) {
}
