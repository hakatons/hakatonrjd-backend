package ru.nikitavov.ticketai.message;

public record AuthRequest(String login, String password) {

}
