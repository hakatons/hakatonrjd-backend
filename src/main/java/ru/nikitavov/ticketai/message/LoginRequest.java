package ru.nikitavov.ticketai.message;

public record LoginRequest(String login, String password) {

}
