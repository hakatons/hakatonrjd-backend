package ru.nikitavov.ticketai.database.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "card_items")
public class CardItem implements IEntity<Integer> {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "card_id", nullable = false)
    Card card;

    @ManyToOne(optional = false)
    @JoinColumn(name = "dish_id", nullable = false)
    Dish dish;

    @Column(name = "amount", nullable = false)
    Integer amount;
}
