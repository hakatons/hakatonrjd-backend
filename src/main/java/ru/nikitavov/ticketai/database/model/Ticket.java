package ru.nikitavov.ticketai.database.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tickets")
public class Ticket {

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "train_id", nullable = false)
    Train train;

    @ManyToOne(optional = false)
    @JoinColumn(name = "train_thread_id", nullable = false)
    TrainThread thread;

    @Column(name = "day")
    Integer day;

    @Column(name = "month", nullable = false)
    Integer month;

    @Column(name = "month_buy", nullable = false)
    Integer monthBuy;

    @Column(name = "year", nullable = false)
    Integer year;

    @Column(name = "year_buy", nullable = false)
    Integer yearBuy;

    @ManyToOne(optional = false)
    @JoinColumn(name = "station_start_id", nullable = false)
    Station stationStart;

    @ManyToOne(optional = false)
    @JoinColumn(name = "station_end_id", nullable = false)
    Station stationEnd;

    @ManyToOne(optional = false)
    @JoinColumn(name = "train_type_id", nullable = false)
    TrainType trainType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ticket_class_id", nullable = false)
    TicketClass ticketClass;

    @Column(name = "place_count", nullable = false)
    Integer placeCount;

    @Column(name = "seat_price", nullable = false)
    Integer seatPrice;

    @Column(name = "distance", nullable = false)
    Integer distance;

//    @Column(name = "price", nullable = false)
//    Integer price;
//
//    @Column(name = "service_price", nullable = false)
//    Integer servicePrice;
//
//    @Column(name = "ticket_price", nullable = false)
//    Integer ticketPrice;
}
