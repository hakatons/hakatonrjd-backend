package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.Dish;

public interface DishRepository extends JpaRepository<Dish, Integer> {
}