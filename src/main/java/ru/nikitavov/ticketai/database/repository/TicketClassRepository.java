package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.TicketClass;

public interface TicketClassRepository extends NameRepository<TicketClass, Integer> {
}