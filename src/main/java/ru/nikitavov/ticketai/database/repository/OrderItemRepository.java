package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {
}