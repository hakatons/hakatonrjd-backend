package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.UserTrainInfo;

public interface UserTrainInfoRepository extends JpaRepository<UserTrainInfo, Integer> {
}