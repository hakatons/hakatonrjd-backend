package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nikitavov.ticketai.database.model.Ticket;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    List<Ticket> findDistinctByYearAndMonth(Integer year, Integer month);

    Ticket findTopById(int id);
}