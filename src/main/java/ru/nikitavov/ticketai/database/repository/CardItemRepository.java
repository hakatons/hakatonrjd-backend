package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.CardItem;

import java.util.Optional;

public interface CardItemRepository extends JpaRepository<CardItem, Integer> {
    Optional<CardItem> findByCard_IdAndDish_Id(Integer cardId, Integer dishId);
}