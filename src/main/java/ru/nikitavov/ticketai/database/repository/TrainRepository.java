package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.Train;

import java.util.Optional;

public interface TrainRepository extends NameRepository<Train, Integer> {
}