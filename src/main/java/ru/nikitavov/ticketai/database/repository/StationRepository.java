package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.Station;

public interface StationRepository extends NameRepository<Station, Integer> {
}