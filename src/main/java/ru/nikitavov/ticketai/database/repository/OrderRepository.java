package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.Order;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findOrderByUser_Id(int userId);
}