package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.TrainType;

public interface TrainTypeRepository extends NameRepository<TrainType, Integer> {
}