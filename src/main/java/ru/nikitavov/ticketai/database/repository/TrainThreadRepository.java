package ru.nikitavov.ticketai.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nikitavov.ticketai.database.model.TrainThread;

public interface TrainThreadRepository extends NameRepository<TrainThread, Integer> {
}