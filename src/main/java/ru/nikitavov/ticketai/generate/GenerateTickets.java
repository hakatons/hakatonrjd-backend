package ru.nikitavov.ticketai.generate;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class GenerateTickets {
    public static void main(String[] args) {
        String csvFile = "generated_data.csv";
        Random rand = new Random();
        int lastId = 0;
        try (FileWriter writer = new FileWriter(csvFile)) {
            for (int i = 2000; i <= 2023; i++) {
                for (int j = 1; j <= 12; j++) {
                    for (int k = 1; k <= 31; k++) {
                        int count = rand.nextInt(1500) + 600;
                        writer.append(Integer.toString(++lastId)).append(",").append(String.valueOf(k)).append(",1790,")
                                .append(String.valueOf(j)).append(",1,").append(String.valueOf(count))
                                .append(",868,").append(String.valueOf(i)).append(",2022,2,1,1,1,1,1\n");
                    }
                }
//                int day = rand.nextInt(30) + 1; // случайное число от 14 до 16
//                int month = rand.nextInt(12) + 1; // случайное число от 14 до 16
//                int count = rand.nextInt(6) + 1; // случайное число от 1 до 4
//                writer.append(Integer.toString(++lastId)).append(",").append(String.valueOf(day)).append(",1790,")
//                        .append(String.valueOf(month)).append(",1,").append(String.valueOf(count)).append(",868,2022,2022,2,1,1,1,1,1\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
