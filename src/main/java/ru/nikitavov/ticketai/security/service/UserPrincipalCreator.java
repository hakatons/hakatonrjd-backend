package ru.nikitavov.ticketai.security.service;

import ru.nikitavov.ticketai.database.model.Role;
import ru.nikitavov.ticketai.database.model.User;
import ru.nikitavov.ticketai.database.repository.UserRepository;
import ru.nikitavov.ticketai.security.data.UserPrincipal;
import ru.nikitavov.ticketai.security.util.RoleUtil;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UserPrincipalCreator {
    private final UserRepository userRepository;

    public UserPrincipalCreator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserPrincipal create(User user) {
        user = userRepository.findById(user.getId()).get();
        Set<Role> roles = user.getRoles();
        List<GrantedAuthority> authorities = RoleUtil.rolesToGrantedAuthority(roles);
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        return new UserPrincipal(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                authorities
        );
    }
}
