package ru.nikitavov.ticketai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.nikitavov.ticketai.database.repository.NameRepository;
import ru.nikitavov.ticketai.security.AppProperties;
import ru.nikitavov.ticketai.service.dl.TicketDemandPrediction;
import ru.nikitavov.ticketai.service.sheet.service.ParseDataset;

@EnableJpaRepositories(
)
@EnableConfigurationProperties({AppProperties.class})
@SpringBootApplication
public class TicketAiApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TicketAiApplication.class, args);

//        context.getBean(ParseDataset.class).parse();
//        context.getBean(TicketDemandPrediction.class).main();
    }

}
