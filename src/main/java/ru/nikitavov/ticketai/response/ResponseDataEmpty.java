package ru.nikitavov.ticketai.response;

public class ResponseDataEmpty extends ResponseDataAbstract<Object> {
    public ResponseDataEmpty() {
        super(null);
    }
}
